from .user import bp as user_bp
from .object import bp as object_bp
from .domain import bp as domain_bp
from .misc import bp as misc_bp

__all__ = ['user_bp', 'object_bp', 'domain_bp', 'misc_bp']
