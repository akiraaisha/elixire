from setuptools import setup

setup(
    name='elixire',
    version='2018.2.0.0',
    description='Image host',
    url='https://elixi.re',
    author='Ave Ozkal, Luna Mendes, Mary Strodl, slice',
    python_requires='>=3.6'
)
