from .ban import setup as ban
from .files import setup as files
from .find import setup as find
from .user import setup as user
