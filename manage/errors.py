class PrintException(Exception):
    pass

class ArgError(Exception):
    pass
